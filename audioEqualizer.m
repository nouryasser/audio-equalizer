classdef finalDSPProject < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                  matlab.ui.Figure
        SampleRateEditField       matlab.ui.control.NumericEditField
        SampleRateEditFieldLabel  matlab.ui.control.Label
        DoneButton                matlab.ui.control.Button
        TypeofFilterButtonGroup   matlab.ui.container.ButtonGroup
        FIRButton_2               matlab.ui.control.RadioButton
        IIRButton                 matlab.ui.control.RadioButton
        FIRButton                 matlab.ui.control.RadioButton
        AdjusttheGainsfortheFollowingFiltersLabel  matlab.ui.control.Label
        k14kHzSlider              matlab.ui.control.Slider
        k14kHzSliderLabel         matlab.ui.control.Label
        k16kHzSlider              matlab.ui.control.Slider
        k16kHzSliderLabel         matlab.ui.control.Label
        k12kHzSlider              matlab.ui.control.Slider
        k12kHzSliderLabel         matlab.ui.control.Label
        k6kHzSlider               matlab.ui.control.Slider
        k6kHzSliderLabel          matlab.ui.control.Label
        k3kHzSlider               matlab.ui.control.Slider
        k3kHzLabel                matlab.ui.control.Label
        kHzSlider                 matlab.ui.control.Slider
        kHzSliderLabel            matlab.ui.control.Label
        HzSlider_3                matlab.ui.control.Slider
        HzLabel_3                 matlab.ui.control.Label
        HzSlider_2                matlab.ui.control.Slider
        HzLabel                   matlab.ui.control.Label
        HzSlider                  matlab.ui.control.Slider
        HzLabel_2                 matlab.ui.control.Label
        AudioEqualizerPanel       matlab.ui.container.Panel
        BrowseWaveFileButton      matlab.ui.control.Button
    end


    properties (Access = private)
        y % Description
        gain1, gain2, gain3, gain4, gain5, gain6, gain7, gain8, gain9
        num1, den1, num2, den2, num3, den3, num4, den4, num5, den5, num6, den6, num7, den7, num8, den8,num9, den9
        samplingRate, fs
    end


    % Callbacks that handle component events
    methods (Access = private)

        % Button pushed function: BrowseWaveFileButton
        function BrowseWaveFileButtonPushed(app, event)
            which('audioread', '-all')
            [file, folder]= uigetfile('*.wav');
            fullName=fullfile(folder, file);
            [app.y,app.fs]=audioread(fullName);
            
        end

        % Value changed function: HzSlider
        function HzSliderValueChanged(app, event)
            app.gain1 = app.HzSlider.Value;
        end

        % Value changed function: HzSlider_2
        function HzSlider_2ValueChanged(app, event)
            app.gain2 = app.HzSlider_2.Value;
        end

        % Value changed function: HzSlider_3
        function HzSlider_3ValueChanged(app, event)
            app.gain3 = app.HzSlider_3.Value;
        end

        % Value changed function: kHzSlider
        function kHzSliderValueChanged(app, event)
            app.gain4 = app.kHzSlider.Value;
        end

        % Value changed function: k3kHzSlider
        function k3kHzSliderValueChanged(app, event)
            app.gain5 = app.k3kHzSlider.Value;
        end

        % Value changed function: k6kHzSlider
        function k6kHzSliderValueChanged(app, event)
            app.gain6 = app.k6kHzSlider.Value;
        end

        % Value changed function: k12kHzSlider
        function k12kHzSliderValueChanged(app, event)
            app.gain7 = app.k12kHzSlider.Value;
        end

        % Value changed function: k14kHzSlider
        function k14kHzSliderValueChanged(app, event)
            app.gain8 = app.k14kHzSlider.Value;
        end

        % Value changed function: k16kHzSlider
        function k16kHzSliderValueChanged(app, event)
            app.gain9 = app.k16kHzSlider.Value;
        end

        % Selection changed function: TypeofFilterButtonGroup
        function TypeofFilterButtonGroupSelectionChanged(app, event)
            selectedButton = app.TypeofFilterButtonGroup.SelectedObject;
            if(selectedButton == app.FIRButton_2)
                app.num1 = fir1(64, (2*170)./app.samplingRate,'low');
                app.num2 = fir1(64,[2*170/app.samplingRate,2*310/app.samplingRate],'low');
                app.num3 = fir1(64,[2*310/app.samplingRate,2*610/app.samplingRate],'bandpass');
                app.num4 = fir1(64,[2*600/app.samplingRate,2*1000/app.samplingRate],'bandpass');
                app.num5 = fir1(64,[2*1000/app.samplingRate,2*3000/app.samplingRate],'bandpass');
                app.num6 = fir1(64,[2*3000/app.samplingRate,2*6000/app.samplingRate],'bandpass');
                app.num7 = fir1(64,[2*6000/app.samplingRate,2*12000/app.samplingRate],'bandpass');
                app.num8 = fir1(64,[2*12000/app.samplingRate,2*14000/app.samplingRate],'bandpass');
                app.num9 = fir1(64,[2*14000/app.samplingRate,2*16000/app.samplingRate],'bandpass');
                app.den1 = 1;
                app.den2 = 1;
                app.den3 = 1;
                app.den4 = 1;
                app.den5 = 1;
                app.den6 = 1;
                app.den7 = 1;
                app.den8 = 1;
                app.den9 = 1;
            end
            if(selectedButton == app.IIRButton)
                [app.num1, app.den1] = butter(3, (2*170)./app.samplingRate,'low');
                [app.num2, app.den2] = butter(3, [2*170/app.samplingRate,2*310/app.samplingRate],'bandpass');
                [app.num3, app.den3] = butter(3, [2*310/app.samplingRate,2*600/app.samplingRate],'bandpass');
                [app.num4, app.den4] = butter(3, [2*600/app.samplingRate,2*1000/app.samplingRate],'bandpass');
                [app.num5, app.den5] = butter(3, [2*1000/app.samplingRate,2*3000/app.samplingRate],'bandpass');
                [app.num6, app.den6] = butter(3, [2*3000/app.samplingRate,2*6000/app.samplingRate],'bandpass');
                [app.num7, app.den7] = butter(3, [2*6000/app.samplingRate,2*12000/app.samplingRate],'bandpass');
                [app.num8, app.den8] = butter(3, [2*12000/app.samplingRate,2*14000/app.samplingRate],'bandpass');
                [app.num9, app.den9] = butter(3, [2*14000/app.samplingRate,2*16000/app.samplingRate],'bandpass');


            end

        end

        % Callback function
        function SampleRateEditFieldValueChanged(app, event)

        end

        % Button pushed function: DoneButton
        function DoneButtonPushed(app, event)

            %plot of first filter
            figure;
            freqz(app.num1,app.den1);
            title('0 - 170 Hz filter');

            figure;
            subplot(3,3,1);
            output1 = filter(app.num1,app.den1,app.y);
            plot(output1);
            title('Wav signal after filter#1 in time domain:');
            outputf1 = fftshift(fft(output1));
            t1=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf1));
            subplot(3,3,2);
            plot(t1,outputf1);
            title('Wav signal after filter#1 in frequency domain:');
            [z,p,k]=tf2zpk(app.num1,app.den1);
            [h,t] = impz(app.num1,app.den1);
            subplot(3,3,3);
            plot(h);
            title('Impulse response');
            n=filtord(app.num1,app.den1);
            disp(n);
            [h,t] = stepz(app.num1,app.den1);
            subplot(3,3,4);
            plot(h);
            title('Step response');
            outputg1=app.gain1*output1;
            subplot(3,3,5);
            plot(outputg1);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf1=fftshift(fft(outputg1));
            t1=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf1));
            subplot(3,3,6);
            plot(t1,outputgf1);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %plot of second filter
            output2=filter(app.num2,app.den2,app.y);
            figure;
            freqz(app.num2,app.den2);
            title('170 - 310 Hz filter');

            figure;
            subplot(3,3,1)
            plot(output2);
            title('Wav signal after filter#2 in time domain:');
            outputf2=fftshift(fft(output2));
            t2=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf2));
            subplot(3,3,2)
            plot(t2,outputf2);
            title('Wav signal after filter#2 in frequency domain:');
            [z,p,k]=tf2zpk(app.num2,app.den2);
            [h,t] = impz(app.num2,app.den2);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num2,app.den2);
            %             disp(n);
            [h,t] = stepz(app.num2,app.den2);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg2=app.gain2*output2;
            subplot(3,3,5)
            plot(outputg2);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf2=fftshift(fft(outputg2));
            t2=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf2));
            subplot(3,3,6)
            plot(t2,outputgf2);
            title('Wav signal after multiplying it by its gain in frequency domain:')
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %Plot of third filter
            output3=filter(app.num3,app.den3,app.y);
            figure;
            freqz(app.num3,app.den3);
            title('310 - 600 Hz filter');

            figure;
            subplot(3,3,1)
            plot(output3);
            title('Wav signal after filter#3 in time domain:')
            outputf3=fftshift(fft(output3));
            t3=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf3));
            subplot(3,3,2)
            plot(t3,outputf3);
            title('Wav signal after filter#3 in frequency domain:')
            [z,p,k]=tf2zpk(app.num3,app.den3);
            [h,t] = impz(app.num3,app.den3);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num3,app.den3);
            disp(n);
            [h,t] = stepz(app.num3,app.den3);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg3=app.gain3*output3;
            subplot(3,3,5)
            plot(outputg3);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf3=fftshift(fft(outputg3));
            t3=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf3));
            subplot(3,3,6)
            plot(t3,outputgf3);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %Plot of fourth filter

            output4=filter(app.num4,app.den4,app.y);
            figure;
            freqz(app.num4,app.den4);
            title('600 - 1k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output4);
            title('Wav signal after filter#4 in time domain:')
            outputf4=fftshift(fft(output4));
            t4=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf4));
            subplot(3,3,2)
            plot(t4,outputf4);
            title('Wav signal after filter#4 in frequency domain:')
            [z,p,k]=tf2zpk(app.num4,app.den4);
            [h,t] = impz(app.num4,app.den4);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num4,app.den4);
            disp(n);
            [h,t] = stepz(app.num4,app.den4);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg4=app.gain4*output4;
            subplot(3,3,5)
            plot(outputg4);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf4=fftshift(fft(outputg4));
            t4=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf4));
            subplot(3,3,6)
            plot(t4,outputgf4);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %Plot of fifth filter
            output5=filter(app.num5,app.den5,app.y);
            figure;
            freqz(app.num5,app.den5);
            title('1k - 3k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output5);
            title('Wav signal after filter#5 in time domain:')
            outputf5=fftshift(fft(output5));
            t5=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf5));
            subplot(3,3,2)
            plot(t5,outputf5);
            title('Wav signal after filter#5 in frequency domain:')
            [z,p,k]=tf2zpk(app.num5,app.den5);
            [h,t] = impz(app.num5,app.den5);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num5,app.den5);
            disp(n);
            [h,t] = stepz(app.num5,app.den5);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg5=app.gain5*output5;
            subplot(3,3,5)
            plot(outputg5);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf5=fftshift(fft(outputg5));
            t5=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf5));
            subplot(3,3,6)
            plot(t5,outputgf5);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %Plot of sixth filter
            output6=filter(app.num6,app.den6,app.y);
            figure;
            freqz(app.num6,app.den6);
            title('3k - 6k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output6);
            title('Wav signal after filter#6 in time domain:')
            outputf6=fftshift(fft(output6));
            t6=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf6));
            subplot(3,3,2)
            plot(t6,outputf6);
            title('Wav signal after filter#6 in frequency domain:')
            [z,p,k]=tf2zpk(app.num6,app.den6);
            [h,t] = impz(app.num6,app.den6);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num6,app.den6);
            disp(n);
            [h,t] = stepz(app.num6,app.den6);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg6=app.gain6*output6;
            subplot(3,3,5)
            plot(outputg6);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf6=fftshift(fft(outputg6));
            t6=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf6));
            subplot(3,3,6)
            plot(t6,outputgf6);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %Plot of 7th filter
            output7=filter(app.num7,app.den7,app.y);
            figure;
            freqz(app.num7,app.den7);
            title('6k - 12k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output7);
            title('Wav signal after filter#7 in time domain:')
            outputf7=fftshift(fft(output7));
            t7=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf7));
            subplot(3,3,2)
            plot(t7,outputf7);
            title('Wav signal after filter#7 in frequency domain:')
            [z,p,k]=tf2zpk(app.num7,app.den7);
            [h,t] = impz(app.num7,app.den7);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num7,app.den7);
            disp(n);
            [h,t] = stepz(app.num7,app.den7);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg7=app.gain7*output7;
            subplot(3,3,5)
            plot(outputg7);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf7=fftshift(fft(outputg7));
            t7=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf7));
            subplot(3,3,6)
            plot(t7,outputgf7);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %plot of 8th filter
            output8=filter(app.num8,app.den8,app.y);
            figure;
            freqz(app.num8,app.den8);
            title('12k - 14k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output8);
            title('Wav signal after filter#8 in time domain:')
            outputf8=fftshift(fft(output8));
            t8=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf8));
            subplot(3,3,2)
            plot(t4,outputf8);
            title('Wav signal after filter#8 in frequency domain:')
            [z,p,k]=tf2zpk(app.num8,app.den8);
            [h,t] = impz(app.num8,app.den8);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num8,app.den8);
            disp(n);
            [h,t] = stepz(app.num8,app.den8);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg8=app.gain8*output8;
            subplot(3,3,5)
            plot(outputg8);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf8=fftshift(fft(outputg8));
            t8=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf8));
            subplot(3,3,6)
            plot(t8,outputgf8);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %plot of 9th filter
            output9=filter(app.num9,app.den9,app.y);
            figure;
            freqz(app.num9,app.den9);
            title('14k - 16k Hz filter');

            figure;
            subplot(3,3,1)
            plot(output9);
            title('Wav signal after filter#9 in time domain:')
            outputf9=fftshift(fft(output9));
            t9=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputf9));
            subplot(3,3,2)
            plot(t9,outputf9);
            title('Wav signal after filter#9 in frequency domain:')
            [z,p,k]=tf2zpk(app.num9,app.den9);
            [h,t] = impz(app.num9,app.den9);
            subplot(3,3,3)
            plot(h);
            title('Impulse response');
            n=filtord(app.num9,app.den9);
            disp(n);
            [h,t] = stepz(app.num9,app.den9);
            subplot(3,3,4)
            plot(h);
            title('Step response');
            outputg9=app.gain9*output9;
            subplot(3,3,5)
            plot(outputg9);
            title('Wav signal after multiplying it by its gain in time domain:');
            outputgf9=fftshift(fft(outputg9));
            t9=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputgf9));
            subplot(3,3,6)
            plot(t9,outputgf9);
            title('Wav signal after multiplying it by its gain in frequency domain:');
            sys = zpk(z,p,k);
            subplot(3,3,7)
            zplane(z,p)

            %composite signal
            outputTotal = outputg1 + outputg2 + outputg3 + outputg4 + outputg5 + outputg6 + outputg7 + outputg8 + outputg9;
            figure;
            subplot(2,2,3);
            plot(outputTotal);
            title('Composite signal in time domain');
            outputTotalf=fftshift(fft(outputTotal));
            tTotal=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputTotalf));
            subplot(2,2,4);
            plot(tTotal,outputTotalf);
            title('Composite signal in frequency domain');
            sound(outputTotal);

            %plot original signal
            subplot(2,2,1);
            plot(app.y);
            title('Original signal in time domain');
            inputf=fftshift(fft(app.y));
            tTotal=linspace((-app.samplingRate/2),(app.samplingRate/2),length(inputf));
            subplot(2,2,2);
            plot(tTotal,inputf);
            title('Original signal in frequency domain');

            %doubling and halving sample
            app.samplingRate = 2*app.samplingRate;
            tTotal=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputTotalf));
            figure;
            plot(tTotal,outputTotalf);
            title('Composite signal in frequency domain after doubling the sampling rate.');
            if(app.fs/2 < 32000)
                outputTotalf = resample(outputTotalf,35000,app.samplingRate);
                app.samplingRate = 35000;
                tTotal=linspace((-app.samplingRate/2),(app.samplingRate/2),length(outputTotalf));
                figure;
                plot(tTotal,outputTotalf);
                title('Composite signal in frequency domain after entering half sampling rate.');
            end
        end

        % Callback function
        function SampleRateEditFieldValueChanged2(app, event)
            %              = app.SampleRateEditField.Value;
        end

        % Value changed function: SampleRateEditField
        function SampleRateEditFieldValueChanged3(app, event)
            app.samplingRate = app.SampleRateEditField.Value;

        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.Position = [100 100 777 535];
            app.UIFigure.Name = 'MATLAB App';

            % Create AudioEqualizerPanel
            app.AudioEqualizerPanel = uipanel(app.UIFigure);
            app.AudioEqualizerPanel.TitlePosition = 'centertop';
            app.AudioEqualizerPanel.Title = 'Audio Equalizer';
            app.AudioEqualizerPanel.FontName = 'Avenir Next Condensed';
            app.AudioEqualizerPanel.FontWeight = 'bold';
            app.AudioEqualizerPanel.FontSize = 20;
            app.AudioEqualizerPanel.Position = [97 427 573 93];

            % Create BrowseWaveFileButton
            app.BrowseWaveFileButton = uibutton(app.AudioEqualizerPanel, 'push');
            app.BrowseWaveFileButton.ButtonPushedFcn = createCallbackFcn(app, @BrowseWaveFileButtonPushed, true);
            app.BrowseWaveFileButton.Position = [227 22 111 22];
            app.BrowseWaveFileButton.Text = 'Browse Wave File';

            % Create HzLabel_2
            app.HzLabel_2 = uilabel(app.UIFigure);
            app.HzLabel_2.HorizontalAlignment = 'right';
            app.HzLabel_2.FontSize = 11;
            app.HzLabel_2.FontWeight = 'bold';
            app.HzLabel_2.FontColor = [0 0 1];
            app.HzLabel_2.Position = [26 221 57 22];
            app.HzLabel_2.Text = '0 - 170 Hz';

            % Create HzSlider
            app.HzSlider = uislider(app.UIFigure);
            app.HzSlider.Limits = [-12 12];
            app.HzSlider.MajorTicks = [-12 1 12];
            app.HzSlider.MajorTickLabels = {'-12', '1', '12'};
            app.HzSlider.Orientation = 'vertical';
            app.HzSlider.ValueChangedFcn = createCallbackFcn(app, @HzSliderValueChanged, true);
            app.HzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.HzSlider.Position = [44 250 7 114];

            % Create HzLabel
            app.HzLabel = uilabel(app.UIFigure);
            app.HzLabel.HorizontalAlignment = 'right';
            app.HzLabel.FontSize = 11;
            app.HzLabel.FontWeight = 'bold';
            app.HzLabel.FontColor = [0 0 1];
            app.HzLabel.Position = [106 221 70 22];
            app.HzLabel.Text = '170 - 310 Hz';

            % Create HzSlider_2
            app.HzSlider_2 = uislider(app.UIFigure);
            app.HzSlider_2.Limits = [-12 12];
            app.HzSlider_2.MajorTicks = [-12 1 12];
            app.HzSlider_2.MajorTickLabels = {'-12', '1', '12'};
            app.HzSlider_2.Orientation = 'vertical';
            app.HzSlider_2.ValueChangedFcn = createCallbackFcn(app, @HzSlider_2ValueChanged, true);
            app.HzSlider_2.MinorTicks = [-11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11];
            app.HzSlider_2.Position = [131 250 7 114];

            % Create HzLabel_3
            app.HzLabel_3 = uilabel(app.UIFigure);
            app.HzLabel_3.HorizontalAlignment = 'right';
            app.HzLabel_3.FontSize = 11;
            app.HzLabel_3.FontWeight = 'bold';
            app.HzLabel_3.FontColor = [0 0 1];
            app.HzLabel_3.Position = [192 221 70 22];
            app.HzLabel_3.Text = '310 - 600 Hz';

            % Create HzSlider_3
            app.HzSlider_3 = uislider(app.UIFigure);
            app.HzSlider_3.Limits = [-12 12];
            app.HzSlider_3.MajorTicks = [-12 1 12];
            app.HzSlider_3.MajorTickLabels = {'-12', '1', '12'};
            app.HzSlider_3.Orientation = 'vertical';
            app.HzSlider_3.ValueChangedFcn = createCallbackFcn(app, @HzSlider_3ValueChanged, true);
            app.HzSlider_3.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.HzSlider_3.Position = [215 250 7 114];

            % Create kHzSliderLabel
            app.kHzSliderLabel = uilabel(app.UIFigure);
            app.kHzSliderLabel.HorizontalAlignment = 'right';
            app.kHzSliderLabel.FontSize = 11;
            app.kHzSliderLabel.FontWeight = 'bold';
            app.kHzSliderLabel.FontColor = [0 0 1];
            app.kHzSliderLabel.Position = [285 221 64 22];
            app.kHzSliderLabel.Text = '600 - 1k Hz';

            % Create kHzSlider
            app.kHzSlider = uislider(app.UIFigure);
            app.kHzSlider.Limits = [-12 12];
            app.kHzSlider.MajorTicks = [-12 1 12];
            app.kHzSlider.Orientation = 'vertical';
            app.kHzSlider.ValueChangedFcn = createCallbackFcn(app, @kHzSliderValueChanged, true);
            app.kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.kHzSlider.Position = [304 250 7 114];

            % Create k3kHzLabel
            app.k3kHzLabel = uilabel(app.UIFigure);
            app.k3kHzLabel.HorizontalAlignment = 'right';
            app.k3kHzLabel.FontSize = 11;
            app.k3kHzLabel.FontWeight = 'bold';
            app.k3kHzLabel.FontColor = [0 0 1];
            app.k3kHzLabel.Position = [373 221 58 22];
            app.k3kHzLabel.Text = '1k - 3k Hz';

            % Create k3kHzSlider
            app.k3kHzSlider = uislider(app.UIFigure);
            app.k3kHzSlider.Limits = [-12 12];
            app.k3kHzSlider.MajorTicks = [-12 1 12];
            app.k3kHzSlider.MajorTickLabels = {'-12', '1', '12'};
            app.k3kHzSlider.Orientation = 'vertical';
            app.k3kHzSlider.ValueChangedFcn = createCallbackFcn(app, @k3kHzSliderValueChanged, true);
            app.k3kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.k3kHzSlider.Position = [384 250 7 114];

            % Create k6kHzSliderLabel
            app.k6kHzSliderLabel = uilabel(app.UIFigure);
            app.k6kHzSliderLabel.HorizontalAlignment = 'right';
            app.k6kHzSliderLabel.FontSize = 11;
            app.k6kHzSliderLabel.FontWeight = 'bold';
            app.k6kHzSliderLabel.FontColor = [0 0 1];
            app.k6kHzSliderLabel.Position = [459 221 58 22];
            app.k6kHzSliderLabel.Text = '3k - 6k Hz';

            % Create k6kHzSlider
            app.k6kHzSlider = uislider(app.UIFigure);
            app.k6kHzSlider.Limits = [-12 12];
            app.k6kHzSlider.MajorTicks = [-12 1 12];
            app.k6kHzSlider.MajorTickLabels = {'-12', '1', '12'};
            app.k6kHzSlider.Orientation = 'vertical';
            app.k6kHzSlider.ValueChangedFcn = createCallbackFcn(app, @k6kHzSliderValueChanged, true);
            app.k6kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.k6kHzSlider.Position = [471 250 7 114];

            % Create k12kHzSliderLabel
            app.k12kHzSliderLabel = uilabel(app.UIFigure);
            app.k12kHzSliderLabel.HorizontalAlignment = 'right';
            app.k12kHzSliderLabel.FontSize = 11;
            app.k12kHzSliderLabel.FontWeight = 'bold';
            app.k12kHzSliderLabel.FontColor = [0 0 1];
            app.k12kHzSliderLabel.Position = [535 221 64 22];
            app.k12kHzSliderLabel.Text = '6k - 12k Hz';

            % Create k12kHzSlider
            app.k12kHzSlider = uislider(app.UIFigure);
            app.k12kHzSlider.Limits = [-12 12];
            app.k12kHzSlider.MajorTicks = [-12 1 12];
            app.k12kHzSlider.MajorTickLabels = {'-12', '1', '12'};
            app.k12kHzSlider.Orientation = 'vertical';
            app.k12kHzSlider.ValueChangedFcn = createCallbackFcn(app, @k12kHzSliderValueChanged, true);
            app.k12kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.k12kHzSlider.Position = [553 250 7 114];

            % Create k16kHzSliderLabel
            app.k16kHzSliderLabel = uilabel(app.UIFigure);
            app.k16kHzSliderLabel.HorizontalAlignment = 'right';
            app.k16kHzSliderLabel.FontSize = 11;
            app.k16kHzSliderLabel.FontWeight = 'bold';
            app.k16kHzSliderLabel.FontColor = [0 0 1];
            app.k16kHzSliderLabel.Position = [681 221 70 22];
            app.k16kHzSliderLabel.Text = '14k - 16k Hz';

            % Create k16kHzSlider
            app.k16kHzSlider = uislider(app.UIFigure);
            app.k16kHzSlider.Limits = [-12 12];
            app.k16kHzSlider.MajorTicks = [-12 1 12];
            app.k16kHzSlider.Orientation = 'vertical';
            app.k16kHzSlider.ValueChangedFcn = createCallbackFcn(app, @k16kHzSliderValueChanged, true);
            app.k16kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.k16kHzSlider.Position = [707 250 7 114];

            % Create k14kHzSliderLabel
            app.k14kHzSliderLabel = uilabel(app.UIFigure);
            app.k14kHzSliderLabel.HorizontalAlignment = 'right';
            app.k14kHzSliderLabel.FontSize = 11;
            app.k14kHzSliderLabel.FontWeight = 'bold';
            app.k14kHzSliderLabel.FontColor = [0 0 1];
            app.k14kHzSliderLabel.Position = [608 221 70 22];
            app.k14kHzSliderLabel.Text = '12k - 14k Hz';

            % Create k14kHzSlider
            app.k14kHzSlider = uislider(app.UIFigure);
            app.k14kHzSlider.Limits = [-12 12];
            app.k14kHzSlider.MajorTicks = [-12 1 12];
            app.k14kHzSlider.Orientation = 'vertical';
            app.k14kHzSlider.ValueChangedFcn = createCallbackFcn(app, @k14kHzSliderValueChanged, true);
            app.k14kHzSlider.MinorTicks = [-12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9 10 11 12];
            app.k14kHzSlider.Position = [631 250 7 114];

            % Create AdjusttheGainsfortheFollowingFiltersLabel
            app.AdjusttheGainsfortheFollowingFiltersLabel = uilabel(app.UIFigure);
            app.AdjusttheGainsfortheFollowingFiltersLabel.FontName = 'Chalkboard SE';
            app.AdjusttheGainsfortheFollowingFiltersLabel.FontSize = 18;
            app.AdjusttheGainsfortheFollowingFiltersLabel.FontWeight = 'bold';
            app.AdjusttheGainsfortheFollowingFiltersLabel.FontColor = [0.7176 0.2745 1];
            app.AdjusttheGainsfortheFollowingFiltersLabel.Position = [195 387 370 28];
            app.AdjusttheGainsfortheFollowingFiltersLabel.Text = 'Adjust the Gains for the Following Filters';

            % Create TypeofFilterButtonGroup
            app.TypeofFilterButtonGroup = uibuttongroup(app.UIFigure);
            app.TypeofFilterButtonGroup.SelectionChangedFcn = createCallbackFcn(app, @TypeofFilterButtonGroupSelectionChanged, true);
            app.TypeofFilterButtonGroup.TitlePosition = 'centertop';
            app.TypeofFilterButtonGroup.Title = 'Type of Filter';
            app.TypeofFilterButtonGroup.Position = [328 75 111 80];

            % Create FIRButton
            app.FIRButton = uiradiobutton(app.TypeofFilterButtonGroup);
            app.FIRButton.Visible = 'off';
            app.FIRButton.Text = 'FIR';
            app.FIRButton.Position = [12 45 40 10];
            app.FIRButton.Value = true;

            % Create IIRButton
            app.IIRButton = uiradiobutton(app.TypeofFilterButtonGroup);
            app.IIRButton.Text = 'IIR';
            app.IIRButton.Position = [12 12 36 22];

            % Create FIRButton_2
            app.FIRButton_2 = uiradiobutton(app.TypeofFilterButtonGroup);
            app.FIRButton_2.Text = 'FIR';
            app.FIRButton_2.Position = [12 33 40 22];

            % Create DoneButton
            app.DoneButton = uibutton(app.UIFigure, 'push');
            app.DoneButton.ButtonPushedFcn = createCallbackFcn(app, @DoneButtonPushed, true);
            app.DoneButton.Position = [340 28 100 22];
            app.DoneButton.Text = 'Done';

            % Create SampleRateEditFieldLabel
            app.SampleRateEditFieldLabel = uilabel(app.UIFigure);
            app.SampleRateEditFieldLabel.HorizontalAlignment = 'right';
            app.SampleRateEditFieldLabel.Position = [286 170 74 22];
            app.SampleRateEditFieldLabel.Text = 'Sample Rate';

            % Create SampleRateEditField
            app.SampleRateEditField = uieditfield(app.UIFigure, 'numeric');
            app.SampleRateEditField.Limits = [32001 Inf];
            app.SampleRateEditField.ValueDisplayFormat = '%.0f';
            app.SampleRateEditField.ValueChangedFcn = createCallbackFcn(app, @SampleRateEditFieldValueChanged3, true);
            app.SampleRateEditField.Position = [375 170 100 22];
            app.SampleRateEditField.Value = 32001;

            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = finalDSPProject

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end
